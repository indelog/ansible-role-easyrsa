#!/bin/bash

#
# Return the numbers of days when the CRL file passed in arg will expire
#

source /usr/local/lib/easyrsa/common_function

crl_enddate="$(openssl crl -nextupdate -in ${1} -noout | cut -d= -f2)" || exit 64
printf $(nb_day_expire "${crl_enddate}")
